import type { NextPage } from 'next'
import Link from 'next/link'
import Head from 'next/head'
import Image from 'next/image'

const Get: NextPage = () => {
  return (
    <div>
      <div className="bg-regal">
        <span>
          <nav className="flex flex-row items-center mx-auto pt-6 w-85 justify-between">
            <div className="flex flex-row items-center w-26 items-center justify-between">
              <Link href="/">
                <a>
                  <span className="leading-23 font-normal text-white tracking-wider text-sm">Zero Gate Wallet</span>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <span className="leading-23 font-normal text-white tracking-wider text-sm">Kayce Labs</span>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <span className="leading-23 font-normal text-white tracking-wider text-sm">Kayce DAO</span>
                </a>
              </Link>
            </div>
            <div>
              <Link href="/">
                <a>
                  <Image
                   
                    src="/Framew.png"
                    width="74px"
                    height="73px"
                  />
                </a>
              </Link>
            </div>
            <div className="flex flex-row items-center w-30 justify-between">
              <Link href="/">
                <a>
                  <span className="leading-25 font-normal text-white tracking-wider text-sm">
                    Get $KAYCE{' '}
                    <Image
                     
                      src="/Arrowwhite.png"
                      width="10.37px"
                      height="10.27px"
                    />
                  </span>
                  <hr className="-mt-0.3 bg-white" />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <span className="leading-25 font-normal text-white tracking-wider text-sm">
                    Dashboard{' '}
                    <Image
                     
                      src="/Arrowwhite.png"
                      width="10.37px"
                      height="10.27px"
                    />
                  </span>
                  <hr className="-mt-0.3 bg-white" />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <span className="leading-25 font-normal text-white tracking-wider text-sm">
                    Launch the DAO{' '}
                    <Image
                     
                      src="/Arrowwhite.png"
                      width="10.37px"
                      height="10.27px"
                    />
                  </span>
                  <hr className="-mt-0.3 bg-white" />
                </a>
              </Link>
            </div>
          </nav>
        </span>
      </div>
      <div>
        <header className="mt-8 w-full pb-16 bg-regal -mt-0.1">
          <div className="mx-auto w-70 justify-center text-center pt-4 pb-6.2 pt-36">
            <h1 className="font-normal w-2/4 mx-auto text-off text-0xxl">Kayce Token Sale is almost here!</h1>
            <p className="font-normal mx-auto w-49 text-off mt-4 text-22xl">
              The governance token of the Kayce DAO:{' '}
              <span className="leading-25 font-bold">$KAYCE </span>
              would be ready for buying and selling very soon.
            </p>
            <div className="flex w-45 mx-auto justify-between mt-8">
              <Link href="/">
                <a className="font-bold text-off mt-16 text-Wbase">
                  <span>Token sale information</span>
                  <hr className="-mt-0.6 bg-off"/>
                </a>
              </Link>
              <Link href="/">
                <a className="font-bold text-off mt-16 text-Wbase">
                  <span>Join the community</span>
                  <hr className="-mt-0.6 bg-off" />
                </a>
              </Link>
            </div>
          </div>
        </header>
      </div>
      <div className="bg-regal -mt-12">
        <Image src="/Groupble.png" height="91.69px" width="1450px" />
      </div>
      <div>
        <section className="bg-regal -mt-32 pt-16">
          <div className="flex flex-row w-85 mx-auto justify-between mt-32 mb-16">
            <div className="w-2/4">
              <h2 className="font-normal text-32xl text-off">PURPOSE</h2>
              <h1 className="font-normal text-off text-35xl w-461 mt-8">
                To open the doors for energy tech innovation in emerging markets
              </h1>
            </div>
            <div className="h-207 -ml-12 border-l-1 border-l-solid border-l-off mt-4"></div>
            <div className="w-2/5">
              <p className="font-normal text-off text-20xal">
                Kayce is a DAO ths removes the risk for energy tecch investment
                in emerging markets. This is accomplished through providing
                financing services and research. Our platform is designed to be
                a self-sustaining public utility. A digital comonns available
                without the possibility of censorship.
              </p>
              <p className="font-normal text-off text-20xal">
                <span className="leading-25 font-bold">
                  We think of it as the infrastructure for the ennrgy transition
                  in emerging markets.{' '}
                </span>
                However, infrastructure requires upkeep; ongoing development,
                maintenance, and support will be needed to ship our ambitious
                roadmap and grow a thriving ecosystem. Funds from token sale of
                $KAYCE,, the Kayce DAO’s native token are entirely for the DAO
                to realize that mission.
              </p>
            </div>
          </div>
        </section>
      </div>
      <div className="bg-regal -mt-40 pt-20">
        <section>
          <h2 className="font-normal text-off text-36xl mt-56 ml-32" >KAYCE is required for:</h2>
          <div className="flex flex-col w-full ml-24">
            <div className="flex flex-row w-full mt-16">
              <div className="bg-vwhite w-40 p-8 h-365 ml-8 border-0.5 border-solid border-off box-border	rounded-xl5 shadow-6xl">
                <h1 className="text-regal font-normal text-lxg w-35 mt-4">Governance Staking</h1>
                <p className="font-normal text-carl text-Zase mt-4">
                  Disputes in the DAO require KAYCE to be ‘staked’ as surety.
                  Winning a dispute earns stakers a share of the loser’s KAYCE.
                </p>
              </div>
              <div className="ml-8 border-l-1 border-solid border-off box-border p-1.3 w-40 mt-4">
                <h1 className="font-normal w-full text-off text-lxg w-40">Revenue Sharing</h1>
                <p className="font-normal text-off text-Zase mt-8">
                  Accounts with both KAYCE & Reputation in the Metacolony may be
                  eligible to claim a share of Metacolony revenue.
                </p>
              </div>
              <div className="ml-8 border-l-1 border-solid border-off box-border p-1.3 w-40 mt-4">
                <h1 className="font-normal w-full text-off text-lxg w-40">Reputation Mining</h1>
                <p className="font-normal text-off text-Zase mt-8">
                  Reputation updates are calculated off-chain by miners who
                  stake KAYCE to compete to perform them to earn KAYCE and
                  Reputation.
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div>
        <section className="-mt-16 pt-60 bg-regal pb-24">
          <div className="w-2/4 mx-auto text-center">
            <h1 className="pt-28 text-center text-white text-35xl">Be a part of changing the world</h1>
            <p className="font-normal text-center text-white text-1411mxl">
              Keep energy researches afloat with us as we build a future for
              context-based energy in emerging markets
            </p>
            <div className="flex flex-row w-94 justify-between mt-20">
              <div>
                <Image src="/whiteDs.png" height="46px" width="44px" />
              </div>
              <div>
                <Image src="/whiteTL.png" height="46px" width="44px" />
              </div>
              <div>
                <Image src="/whiteMD.png" height="46px" width="44px" />
              </div>
              <div>
                <Image src="/whiteYT.png" height="46px" width="44px" />
              </div>
              <div>
                <Image src="/whiteTW.png" height="46px" width="44px" />
              </div>
            </div>
          </div>
        </section>
      </div>
      <footer>
        <div className="flex flex-col justify-between w-full mx-auto bg-regal pb-8  pt-20">
          <div className="flex flex-row w-full justify-between mx-auto mt-12 -ml-8">
            <div className="flex flex-col w-2/4 ml-12">
              <div className="flex flex-row w-85 mx-auto justify-between">
                <div>
                  <h5 className="font-semibold text-white text-Zase">Offices</h5>
                  <p className="font-normal text-off text-Zbase mt-8">London:</p>
                  <p className="font-normal text-off text-Zbase">One Heddon St, Regent Street,</p>
                  <p className="font-normal text-off text-Zbase">London, W1B 4BD</p>
                </div>
                <div className="mt-16">
                  <p className="font-normal text-off text-Zbase">Spain:</p>
                  <p className="font-normal text-off text-Zbase">6 Carrer de l’Architecte, </p>
                  <p className="font-normal text-off text-Zbase">18016, Barcelona.</p>
                </div>
              </div>
              <div className="flex flex-row w-90 mx-auto justify-between ml-10 mt-40">
                <Link href="/">
                  <a>
                    <h5 className="font-semibold text-white text-Zase">Kayce DAO</h5>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <h5 className="font-semibold text-white text-Zase">Imprint</h5>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <h5 className="font-semibold text-white text-Zase">Whitepaper</h5>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <h5 className="font-semibold text-white text-Zase">Apply for Funding</h5>
                  </a>
                </Link>
              </div>
            </div>
            <div className="flex flex-col w-2/4 ml-12">
              <div className="flex flex-row w-85 mx-auto justify-between">
                <div>
                  <h5 className="font-semibold text-white text-Zase">Support</h5>
                  <Link href="/">
                    <a>
                      <p className="font-normal text-off text-Zbase mt-8">email@kayce.com</p>
                    </a>
                  </Link>
                  <hr className="border-b-off border-solid border-b-1"/>
                </div>
                <div className="mt-20">
                  <Image src="/bglogo.png" height="121px" width="151px" />
                </div>
              </div>
              <div className="flex flex-row w-90 mx-auto justify-between ml-10 mt-24">
                <Link href="/">
                  <a>
                    <span className="font-semibold text-off text-xg">
                      Dashboard{' '}
                      <Image
                       
                        src="/Arrowwhite.png"
                        width="10.37px"
                        height="10.27px"
                      />
                    </span>
                    <hr className="-mt-0 border-b-off border-solid border-b-1"/>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <span className="font-semibold text-off text-xg">
                      Get $KAYCE{' '}
                      <Image
                       
                        src="/Arrowwhite.png"
                        width="10.37px"
                        height="10.27px"
                      />
                    </span>
                    <hr className="-mt-0 border-b-off border-solid border-b-1"/>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <span className="font-semibold text-off text-xg">
                      Launch the DAO{' '}
                      <Image
                       
                        src="/Arrowwhite.png"
                        width="10.37px"
                        height="10.27px"
                      />
                    </span>
                    <hr className="-mt-0 border-b-off border-solid border-b-1"/>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="font-normal text-lateral text-xa ml-14 mt-8">
            <p className="text-off ">© 2022 Kayce. All rights reserved</p>
          </div>
        </div>
      </footer>
    </div>
  )
}

export default Get
