import Link from 'next/link'
import Image from 'next/image'

const Modal = () => {
  return (
    <div>
      <section className="w-80 mx-auto mt-20 h-732 bg-crater blur-xs">
        <div className="mx-auto pt-24 w-70">
          <span className="absolute left-52 top-7.1 ">
            <Image src="/x.png" height="16px" width="16px" />
          </span>
          <form>
            <div className="flex flex-row bg-regal rounded-x7 p-8">
              <div className="flex flex-col w-42">
                <h1 className="font-normal text-36xl text-corhal">Join Grant</h1>
                <div className="w-full">
                  <h3 className="font-semibold text-white text-15mxl ">Name *</h3>
                  <label>
                    <input className="w-416 h-39 bg-dust opacity-50 rounded-tr-5 rounded-tl-7 w-full border-transparent" type="text" />
                  </label>
                  <hr className="border-b-1 border-solid	border-white mt-0"/>
                </div>
                <div className="w-full">
                  <h3 className="font-semibold text-white text-15mxl ">Email *</h3>
                  <label>
                    <input className="w-416 h-39 bg-dust opacity-50 rounded-tr-5 rounded-tl-7 w-full border-transparent" type="text" />
                  </label>
                  <hr className="border-b-1 border-solid	border-white mt-0" />
                </div>
                <div className="w-full">
                  <h3 className="font-semibold text-white text-15mxl ">LinkedIn / Professional URL*</h3>
                  <label>
                    <input className="w-416 h-39 bg-dust opacity-50 rounded-tr-5 rounded-tl-7 w-full border-transparent" type="text" />
                  </label>
                  <hr className="border-b-1 border-solid border-white mt-0" />
                </div>
              </div>
              <div className="flex flex-col w-42">
                <div className="w-full">
                  <h3 className="ml-2.4 mt-8.5">
                    Motivation for Joining The Project *
                  </h3>
                  <label>
                    <input className="h-151 bg-dust opacity-50 rounded-tr-5 rounded-tl-7 w-125 border-transparent ml-2.4	" type="text" />
                  </label>
                  <hr className="w-125 ml-2.4"/>
                </div>
                <div className="mt-24 w-140">
                  <Link href="/">
                    <a className="font-semibold text-xg text-white ml-56">
                      <span>
                        Submit Application
                        <Image
                          
                          src="/Arrowwhite.png"
                          width="10.37px"
                          height="10.27px"
                        />
                      </span>
                      <hr className="mt-0 bg-lol w-2/5 ml-56" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </div>
  )
}

export default Modal
