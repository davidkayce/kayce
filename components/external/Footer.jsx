import Link from 'next/link'
import Image from 'next/image'

const Footer = () => {
  return (
    <>
      <footer>
        <div className="flex flex-col justify-between w-full mx-auto bg-element mt-20 mb-8">
          <div className="flex flex-row w-full justify-between mx-auto mt-12 -mb-8">
            <div className="flex flex-col w-2/4 ml-12">
              <div className="flex flex-row w-85 mx-auto justify-between">
                <div>
                  <h5 className="font-semibold text-regal text-xg">Offices</h5>
                  <p className="font-normal text-regal text-Zbase mt-8">London:</p>
                  <p className="font-normal text-regal text-Zbase">One Heddon St, Regent Street,</p>
                  <p className="font-normal text-regal text-Zbase">London, W1B 4BD</p>
                </div>
                <div className="mt-16">
                  <p>Spain:</p>
                  <p>6 Carrer de l’Architecte, </p>
                  <p>18016, Barcelona.</p>
                </div>
              </div>
              <div className="flex flex-row w-90 mx-auto justify-between ml-10 mt-40">
                <Link href="/">
                  <a>
                    <h5>Kayce DAO</h5>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <h5>Imprint</h5>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <h5>Whitepaper</h5>
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <h5>Apply for Funding</h5>
                  </a>
                </Link>
              </div>
            </div>
            <div className="flex flex-col w-2/4 ml-12">
              <div className="flex flex-row mx-auto justify-between w-85">
                <div>
                  <h5 className="font-semibold text-regal text-xg">Support</h5>
                  <Link href="/">
                    <a>
                      <p className="font-normal text-regal text-Zbase mt-8">email@kayce.com</p>
                    </a>
                  </Link>
                  <hr/>
                </div>
                <div className="mt-20">
                  <Image src="/imagefooter.png" height="121px" width="151px" />
                </div>
              </div>
              <div className="flex flex-row mx-auto w-90 justify-between ml-10 mt-20">
                <Link href="/">
                  <a>
                    <span className="font-semibold text-xg text-regal">
                      Dashboard{' '}
                      <Image
                        src="/Arrow.png"
                        width="10.37px"
                        height="10.27px"
                      />
                    </span>
                    <hr className="-mt-0" />
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <span>
                      Get $KAYCE{' '}
                      <Image
                        
                        src="/Arrow.png"
                        width="10.37px"
                        height="10.27px"
                      />
                    </span>
                    <hr className="-mt-0" />
                  </a>
                </Link>
                <Link href="/">
                  <a>
                    <span>
                      Launch the DAO{' '}
                      <Image
                        
                        src="/Arrow.png"
                        width="10.37px"
                        height="10.27px"
                      />
                    </span>
                    <hr className="-mt-0 bg-yeag" />
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="font-normal text-14mxl text-lateral mt-16 ml-20">
            <p>© 2022 Kayce. All rights reserved</p>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer
