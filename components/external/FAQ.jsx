import Image from 'next/image'

const Faq = () => {
  return (
    <div className="pb-20">
      <section className="flex flex-col w-80 mx-auto mt-20">
        <h1 className="font-normal text-4xl align-center text-regal">FAQs</h1>
        <div>
          <div className="flex flex-row w-full justify-between">
            <div>
              <p className="font-normal text-qst text-carl">Who can apply? </p>
            </div>
            <div className="mt-10">
              <Image src="/up.png" height="11.02px" width="17.28px" />
            </div>
          </div>
          <div className="flex flex-row w-full justify-between">
            <div>
                <p className="font-normal text-20xxl text-carl w-61">
                  All researchers are welcome to apply, including students. You
                  should have access to appropriate facilities for the work you
                  propose. We prioritise projects with a clear pathway to
                  generating intellectual property and potential commercial
                  value. Not sure whether your project is a fit? Our discord
                  community can help.
                </p>
            </div>
          </div>
          <div className="flex flex-row w-full justify-between">
            <div>
              <p className="font-normal text-qst text-carl">What kind of projects/companies do you finance?</p>
            </div>
            <div className="mt-10">
              <Image src="/path.png" height="11.02px" width="17.28px" />
            </div>
          </div>
          <hr className="-mt-4 bg-qoral mix-blend-normal opacity-67" />
          <div className="flex flex-row w-full justify-between">
            <div>
              <p className="font-normal text-qst text-carl">Who owns IP from funded projects?</p>
            </div>
            <div className="mt-10">
              <Image src="/path.png" height="11.02px" width="17.28px" />
            </div>
          </div>
          <hr className="-mt-4 bg-qoral mix-blend-normal opacity-67" />
          <div className="flex flex-row w-full justify-between">
            <div>
              <p className="font-normal text-qst text-carl">What project stages do you finance ?</p>
            </div>
            <div className="mt-10">
              <Image src="/path.png" height="11.02px" width="17.28px" />
            </div>
          </div>
          <hr className="-mt-4 bg-qoral mix-blend-normal opacity-67"/>
        </div>
      </section>
    </div>
  )
}

export default Faq
