import Link from "next/link";
import Image from 'next/image'

const Navbar = () => {
    return (
        <>
            <nav className="flex flex-row items-center mx-auto mt-6 w-85 justify-between">
                <div className="flex flex-row items-center w-26 items-center justify-between">
                    <Link href = '/'>
                        <a>
                            <span className="leading-23 font-normal	text-sm	tracking-wider text-regal">Zero Gate Wallet</span>
                        </a>
                    </Link>
                    <Link href = '/'>
                        <a>
                            <span className="leading-23 font-normal	text-sm	tracking-wider text-regal">Kayce Labs</span>
                        </a>
                    </Link>
                    <Link href = '/'>
                        <a>
                            <span className="leading-23 font-normal	text-sm	tracking-wider text-regal">Kayce DAO</span>
                        </a>
                    </Link>
                </div>
                <div>
                    <Link href = '/'>
                        <a>
                            <Image src="/Nav.jpg" width="74px" height="73px"/>
                        </a>
                    </Link>
                </div>
                <div className="flex flex-row items-center w-30 justify-between">
                    <Link href = '/'>
                        <a>
                            <span className="leading-25 font-normal	text-sm	tracking-wider text-regal" >Get $KAYCE <Image src="/Arrow.png" width="10.37px" height="10.27px"/></span>
                            <hr className="-mt-0.3"/>
                        </a>
                    </Link>
                    <Link href = '/'>
                        <a>
                            <span className="leading-25 font-normal	text-sm	tracking-wider text-regal" >Dashboard <Image src="/Arrow.png" width="10.37px" height="10.27px"/></span>
                            <hr className="-mt-0.3"/>
                        </a>
                    </Link>
                    <Link href = '/'>
                        <a>
                            <span className="leading-25 font-normal	text-sm	tracking-wider text-regal" >Launch the DAO <Image src="/Arrow.png" width="10.37px" height="10.27px"/></span>
                            <hr className="-mt-0.3"/>
                        </a>
                    </Link>
                </div>
            </nav>
        </>
    )
}


export default Navbar;